export const CONFIG = {
	port: process.env.PORT || 4333,
	host: process.env.HOST || '0.0.0.0',
};

export default CONFIG