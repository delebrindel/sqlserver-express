import express from 'express'
import DEBUG_VARS from './enums/debugger-variables'
import createError from 'http-errors'
import * as path from 'path'
import * as helmet from 'helmet'
import chalk from 'chalk'
const nodeEnv = process.env.NODE_ENV || 'development'

// app
const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'twig')
// This section is optional and used to configure twig.
app.set('twig options', {
    allow_async: true, // Allow asynchronous compiling
    strict_variables: false,
})

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))

app.use(helmet.frameguard())
app.use(helmet.xssFilter())
app.use(helmet.noSniff())
app.use(helmet.ieNoOpen())
app.use(
    helmet.hsts({
        maxAge: 15778476000, // 6 months
        includeSubDomains: true,
        force: true,
    })
)
app.disable('x-powered-by')

app.get('/', (req, res) => res.json({ app: 'Test API' }))

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}
    // render the error page
    console.log(chalk.red(DEBUG_VARS.MESSAGE.DIVIDER))
    console.log(chalk.yellow('Error Found'))
    console.log(chalk.red(DEBUG_VARS.MESSAGE.DIVIDER))
    console.log(chalk.yellow('Msg:     ' + err.message))
    console.log(chalk.red(DEBUG_VARS.MESSAGE.DIVIDER))
    let errorMessage = err.status === 404 ? 'Page not found' : err.message

    res.render('error', { errorType: err.status, errorMessage: errorMessage })
})

export default app
