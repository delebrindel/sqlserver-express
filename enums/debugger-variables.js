const debuggerVariables = {
    LEVEL:{
        ALERT : 'alert',
        WARNING: 'warning',
        SUCCESS : 'success',
        INFO : 'info',
    },
    MESSAGE: {
        DIVIDER: '=============================================================',
        EMPTY: 'No debug message found',
        PAGE_NOT_FOUND: 'Page not found',
        SKIN_NOT_FOUND: 'Invalid skin name',
        CONTENT_NOT_FOUND: 'Entry for the URL not found on contentful',
        LAYOUT_NOT_FOUND: 'Page not found',
    }
}

export default debuggerVariables