USE [master]
GO

IF DB_ID('testingSuite') IS NOT NULL
  set noexec on               -- prevent creation when already exists

/****** Object:  Database [testingSuite]    Script Date: 2020/08/02 ******/
CREATE DATABASE [testingSuite];
GO

USE [testingSuite]
GO

/****** Object:  Table [dbo].[People]    Script Date: 2020/08/02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[People](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NULL,
	[Username] [varchar](50) NOT NULL,
	[SkinId] [int] NULL,
	[Active] [bit] DEFAULT 1
 CONSTRAINT [PK_People] PRIMARY KEY CLUSTERED
(
	[Id] ASC
)) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Skins]    Script Date: 2020/08/02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Skins](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SkinName] [varchar](50) NOT NULL,
	[Active] BIT DEFAULT 1
 CONSTRAINT [PK_Skins] PRIMARY KEY CLUSTERED
(
	[Id] ASC
)) ON [PRIMARY]
GO
-- Insert skins
INSERT [dbo].[Skins] ([SkinName]) VALUES ('Spin And Win')
GO
INSERT [dbo].[Skins] ([SkinName]) VALUES ('Kitty Bingo')
GO
INSERT [dbo].[Skins] ([SkinName]) VALUES ('Codere Casino')
GO
-- Insert users
INSERT [dbo].[People] ([FirstName], [LastName], [Username], [SkinId]) VALUES ('Falso', 'Falserini','Uwishin', 1)
GO
INSERT [dbo].[People] ([FirstName], [LastName], [Username], [SkinId]) VALUES ('Verdadero', 'Verdaderini','Wakamolo', 1)
GO
INSERT [dbo].[People] ([FirstName], [LastName], [Username], [SkinId]) VALUES ('Talveza', 'Talvezerina','Bellacasa', 2)
GO

ALTER TABLE [dbo].[People]  WITH CHECK ADD  CONSTRAINT [FK_People_Skins] FOREIGN KEY([SkinId])
REFERENCES [dbo].[Skins] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[People] CHECK CONSTRAINT [FK_People_Skins]
GO

CREATE LOGIN [testerino]
    WITH PASSWORD = 'dberinoTesterino123!';

CREATE USER [testerino] FOR LOGIN [testerino] WITH DEFAULT_SCHEMA=[dbo]
GO

GRANT SELECT, INSERT, UPDATE ON Skins TO [testerino];
GRANT SELECT, INSERT, UPDATE ON People TO [testerino];
GO